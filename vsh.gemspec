Gem::Specification.new do |s|
  s.name        = 'vsh'
  s.version     = '0.1'
  s.executables << 'vsh'
  s.date        = '2015-10-26'
  s.summary     = 'Simple Vagrant manager'
  s.description = 'A simple Vagrant VM manager'
  s.authors     = ['axocomm']
  s.email       = 'axocomm@gmail.com'
  s.files       = ['lib/vsh/vagrant-vm.rb', 'lib/vsh/util.rb',
                   'lib/vsh/cli.rb', 'lib/vsh.rb']
  s.license       = 'MIT'
  s.add_dependency 'thor', '~> 0.19'
end
