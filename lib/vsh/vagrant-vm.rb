class VagrantVM
  attr_accessor :id, :name, :path, :state, :box

  def initialize(id, name, path, state, box)
    @id, @name, @path, @state, @box = id, name, path, state, box
  end

  def halt
    power :halt
  end

  def reboot
    power :reboot
  end

  def start
    power :start
  end

  def suspend
    power :suspend
  end

  def enter
    do_cmd 'vagrant ssh'
  end

  def destroy
    do_cmd 'vagrant destroy'
  end

  def to_json
    {
      :id    => @id,
      :name  => @name,
      :path  => @path,
      :state => @state,
      :box   => @box
    }.to_json
  end

  def to_s
    ks = %w{ID Name Path State Box}
    vs = [@id, @name, @path, @state, @box]
    width = ks.map { |k| k.size }.max + 10

    ks.zip(vs).map do |k, v|
      sprintf "%-#{width}s%s", (k + ':'), v
    end
  end

  private
  def do_cmd(cmd)
    cmd = "#{cmd} #{@id}"

    pid = fork do
      system cmd
    end

    Process.wait pid
  end

  def power(p)
    case p
    when :halt
      do_cmd 'vagrant halt'
    when :reboot
      do_cmd 'vagrant reload'
    when :start
      do_cmd 'vagrant up'
    when :suspend
      do_cmd 'vagrant suspend'
    else
      puts "Invalid power state '#{p.to_s}'"
    end
  end
end
