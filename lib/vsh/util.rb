require 'json'

module VSH
  def debug?
    options[:debug]
  end

  def generate_vagrantfile(options)
    box = options[:box]
    hostname = options[:hostname]
    ip = options[:ip]
    memory = options[:memory]

    this_file = File.symlink?(__FILE__) ? File.readlink(__FILE__) : __FILE__
    template_file = "#{File.dirname(this_file)}/templates/Vagrantfile.erb"

    template = ERB.new File.read(template_file), nil, '%'
    template.result(binding)
  end

  def get_index(home)
    index_file = "#{home}/.vagrant.d/data/machine-index/index"
    if not File.exists? index_file
      die "#{index_file} does not exist"
    end

    begin
      index_content = File.read index_file
      JSON.parse index_content
    rescue JSON::ParserError => e
      die "get_index: #{e}"
    end
  end

  def get_vms
    die 'machines key not found in index' unless @index.include? 'machines'

    @index['machines'].keys.inject({}) do |cur, k|
      m = @index['machines'][k]
      id = k[0..7]
      path = m['vagrantfile_path']
      name = path.split(/\//)[-1]
      state = m['state']
      begin
        box = m['extra_data']['box']['name']
      rescue
        box = 'unknown'
      end

      cur[name] = VagrantVM.new id, name, path, state, box
      cur
    end
  end

  def get_vm(name)
    die "VM '#{name}' does not exist" unless @vms.include? name
    @vms[name]
  end

  def next_vagrant_ip(prefix)
    lines = File.open('/etc/hosts').readlines
    ips = lines.select do |line|
      /VAGRANT:/.match line
    end.map do |line|
      line.split(/ /)[0]
    end.select do |ip|
      /^#{prefix}/.match ip
    end.uniq

    if ips.empty?
      nil
    else
      last_octet = ips.map { |ip| ip.split(/\./)[-1] }.map(&:to_i).max + 1
      "#{prefix}.#{last_octet}"
    end
  end

  def before_command(options)
    @index = get_index options[:home]
    @vms = get_vms
  end

  def die(message, rc = 1)
    puts message
    exit rc
  end
end
