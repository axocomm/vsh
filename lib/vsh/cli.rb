#!/usr/bin/env ruby

require 'erb'
require 'json'
require 'thor'
require 'vsh/vagrant-vm'
require 'vsh/util'

module VSH
  class Cli < Thor
    include VSH

    class_option :home, :default => Dir.home
    class_option :debug, :type => :boolean, :default => false

    desc 'list', 'List Vagrant VMs'
    def list
      before_command options
      puts @vms.keys.join("\n")
    end

    desc 'status VM', 'Do something with VM (or `all`)'
    option :state
    def status(vm_name = nil)
      before_command options
      if vm_name.nil? or /all/i.match(vm_name)
        puts '%-30s%s' % ['VM Name', 'State']
        puts '%-30s%s' % ['-------', '-----']
        vms = @vms
        if not options[:state].nil?
          vms.select! { |k, m| m.state == options[:state] }
        end

        vms.sort.each { |k, m| printf "%-30s%s\n" % [k, m.state] }
      else
        vm = get_vm vm_name
        puts vm.state
      end
    end

    desc 'power VM STATE', 'Control VM power'
    def power(vm_name, p)
      before_command options
      vm = get_vm vm_name

      case p
      when /halt|(shut)?down|off/i
        vm.halt
      when /re(start|load|boot)/i
        vm.reboot
      when /start|up|on/i
        vm.start
      when /suspend|sleep/i
        vm.suspend
      else
        die "Invalid power command '#{p}'"
      end
    end

    desc 'enter VM', 'Enter a running VM'
    def enter(vm_name)
      before_command options
      vm = get_vm vm_name

      state = vm.state
      unless /running/.match(state)
        die "'#{vm_name}' current state is '#{state}', not 'running'"
      end

      vm.enter
    end

    desc 'create DIRECTORY', 'Create a new Vagrant VM in DIRECTORY'
    option :box, :required => true
    option :hostname, :default => 'vagrant'
    option :ip, :required => true
    option :memory, :default => 1024
    option :force, :type => :boolean, :default => false
    def create(directory)
      if not options[:force] and File.exists? "#{directory}/Vagrantfile"
        die "Vagrantfile already exists in #{directory}"
      end

      content = generate_vagrantfile options

      begin
        Dir.mkdir directory if not Dir.exists? directory
        Dir.chdir(directory) do
          File.write 'Vagrantfile', content
        end

        puts "#{directory}/Vagrantfile written"
      rescue Exception => e
        die "create: #{e}"
      end
    end

    desc 'destroy VM1 VM2...', 'Destroy VMs'
    def destroy(*vm_names)
      before_command options

      vm_names.each do |vm_name|
        say "!!! Destroying #{vm_name}", :bold

        vm = get_vm vm_name

        vm.destroy
      end
    end

    desc 'hosts', 'List Vagrant entries in the hosts file'
    def hosts
      puts '%-20s%s' % ['Hostname', 'IP']
      puts '%-20s%s' % ['--------', '--']
      File.readlines('/etc/hosts').select do |line|
        /VAGRANT/.match line
      end.reject do |line|
        /\.dev$/.match line.split[1]
      end.each do |line|
        puts '%-20s%s' % line.split[0..1].reverse
      end
    end

    desc 'info VM', 'Dump information about VM'
    option :format, :default => 'print'
    def info(vm_name)
      before_command options
      vm = get_vm vm_name

      case options[:format]
      when /json/i
        puts vm.to_json
      else
        puts vm.to_s
      end
    end

    desc 'next-ip', 'Get the next available IP for Vagrant'
    option :prefix, :default => '192.168.56'
    def next_ip
      next_ip = next_vagrant_ip(options[:prefix])
      if next_ip.nil?
        die "No IPs found for #{options[:prefix]} in /etc/hosts"
      end

      puts next_ip
    end

    desc 'prune', 'Reload global-status'
    def prune
      `vagrant global-status --prune`
    end

    desc 'test THING', 'Test the THING'
    option :foo
    def test(thing)
      case thing
      when /options/i
        puts options[:foo].nil?
      else
        puts "Nothing to do for #{thing}"
      end
    end
  end
end
